# play-with-ncnn-dev

Repository for playing the computer vision apps using ncnn C++ API on Raspberry Pi. Source **Python**: https://github.com/mheriyanto/play-with-ncnn

## Build in Linux

**Step1.** Build and install OpenCV from https://github.com/opencv/opencv

**Step2.** Clone NCNN repository

```console
$ git clone --recursive https://github.com/Tencent/ncnn.git
```

Build NCNN following this tutorial: [Build for Linux / NVIDIA Jetson / Raspberry Pi](https://github.com/Tencent/ncnn/wiki/how-to-build#build-for-linux)


**Step3.** Set environment variables. Run:

```console
$ export ncnn_DIR=YOUR_NCNN_PATH/build/install/lib/cmake/ncnn
```

Build project

```console
$ cd play-with-ncnn-dev
$ mkdir build && cd build
$ cmake ..
$ make
```

## Run demo

```console
# Webcam
$ ./NanoDet 0 0 ../models/nanodet_m.param ../models/nanodet_m.bin

# Inference images
$ ./NanoDet 1 ../samples/test.jpg ../models/nanodet_m.param ../models/nanodet_m.bin

# Inference video
$ ./NanoDet 2 VIDEO_PATH ../models/nanodet_m.param ../models/nanodet_m.bin

# Benchmark
$ ./NanoDet 3 0 ../models/nanodet_m.param ../models/nanodet_m.bin
```


## References
+ NanoDet: Super fast and lightweight anchor-free object detection model. [here](https://github.com/RangiLyu/nanodet) and [docs](https://github.com/RangiLyu/nanodet/blob/main/demo_ncnn/README.md)
+ Q-engineering and nihui: [NanoDet on Raspberry Pi 4](https://github.com/Qengineering/NanoDet-ncnn-Raspberry-Pi-4) and [NanoDet on Jetson Nano](https://github.com/Qengineering/NanoDet-ncnn-Jetson-Nano)